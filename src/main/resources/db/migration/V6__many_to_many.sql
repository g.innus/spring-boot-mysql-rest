CREATE TABLE `passenger_visited_countries` (
  `passanger_id` bigint NOT NULL,
  `country_id` bigint NOT NULL,
  PRIMARY KEY (`passanger_id`,`country_id`),
  KEY `FK74vev1isld13mlwus2qgsd4o` (`country_id`),
  CONSTRAINT `FK66n748glj6qt8trkqutebe3g` FOREIGN KEY (`passanger_id`) REFERENCES `pax` (`id`),
  CONSTRAINT `FK74vev1isld13mlwus2qgsd4o` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;